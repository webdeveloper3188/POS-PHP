<?php
/**
 * Pricing slabs and test cases to verification
 * 
 * @author Sudheer Kumar Nayak <webdeveloper3188@gmail.com>
 */
include("Terminal.php");

    /**
     * Pricing slab as an array 
     */
	$pricingSlabs = [
			'A' => [
				'unit_price' => 2,
				'volume_price' => [
						'qty' => 4,
						'price' => 7
				]
			],
			'B' => ['unit_price' => 12],
			'C' => [
				'unit_price' => 1.25,
				'volume_price' => [
					'qty' => 6,
					'price' => 6
				]
			],
			'D' => ['unit_price' => 0.15]
		];

	//Test Case 1
	$terminal = new Terminal();
	$terminal->setPricing($pricingSlabs);
	$terminal->setCurrency('$');
	$terminal->scan('A');
	$terminal->scan('B');
	$terminal->scan('C');
	$terminal->scan('D');
	$terminal->scan('A');
	$terminal->scan('B');
	$terminal->scan('A');
	$terminal->scan('A');
	echo "Result of Test Case 1 (ABCDABAA): " . $terminal->calculateTotal();
	echo "</br>";

	//Test Case 2
	$terminal = new Terminal();
	$terminal->setPricing($pricingSlabs);
	$terminal->setCurrency('$');
	$terminal->scan('C');
	$terminal->scan('C');
	$terminal->scan('C');
	$terminal->scan('C');
	$terminal->scan('C');
	$terminal->scan('C');
	$terminal->scan('C');
	echo "Result of Test Case 2 (CCCCCCC) : " . $terminal->calculateTotal();
	echo "</br>";

	//Test Case 3
	$terminal = new Terminal();
	$terminal->setPricing($pricingSlabs);
	$terminal->setCurrency('$');
	$terminal->scan('A');
	$terminal->scan('B');
	$terminal->scan('C');
	$terminal->scan('D');
	echo "Result of Test Case 3 (ABCD) : " . $terminal->calculateTotal();