<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//display_errors();
/*
 * Complete the 'minimumMoves' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY arr1
 *  2. INTEGER_ARRAY arr2
 */

function minimumMoves($arr1, $arr2) {
    // Initialize a variable to collect difference
    $globalDiff = 0;

    /* Looping through the array's for each of the elements, expecting both array should
        have same number of elements
    */
    for($i = 0; $i < count($arr1); $i++){
        
        // Split each element value into array to start compare
        $input1 = str_split($arr1[$i]);
        $input2 = str_split($arr2[$i]);

        // Looing with element values and compare
        for($x = 0; $x < count($input1); $x++){

            // diff the values and store the difference
            $diff = $input1[$x] - $input2[$x];

            // if difference is non-zero, make it absolute and store the moves
            if($diff !== 0 ){
                $globalDiff += abs($diff);
            }
        }
    }
    return $globalDiff;
}

minimumMoves([123, 345], [321, 546]);
