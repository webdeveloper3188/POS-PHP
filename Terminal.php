<?php
/**
 * Code to get scanned product codes as input, calculate
 * price based on price slab defined and return total with currency Symbol.
 * 
 * @author Sudheer Kumar Nayak <webdeveloper3188@gmail.com>
 */
class Terminal
{
    /**
     * Array of scanned items
     *
     * @var array
     */
	private $_items;
	
    /**
     * Pricing slab
     *
     * @var array
     */
	private $_pricingSlab;

	 /**
     * Currency Symbol
     *
     * @var string
     */
	private $_currency;
	
    /**
     * Total of scanned items
     *
     * @var float
     */
	private $_total;

	/**
	 * Scan a single product
	 *
	 * @param string Product code
	 */
	public function scan(string $productCode)
	{
		$this->_items[] = $productCode;
	}

	/**
	 * Assign Pricing slab
	 *
	 * @param array Pricing slab with unit price and volume price
	 */
	public function setPricing(array $pricingSlab)
	{
		$this->_pricingSlab = $pricingSlab;
	}

	/**
	 * Assign Pricing slab
	 *
	 * @param string Currency Code
	 */
	public function setCurrency(string $currency)
	{
		$this->_currency = $currency;
	}

	/**
	 * Return total with currency symbol
	 *
	 * @param float Total of scanned products
	 * @return string Total with currency symbol
	 */
	private function totalWithCurrencySymbol($total)
	{
		return $this->_currency . number_format($total, 2);
	}

	/**
	 * Returns total of all the product codes scanned with currency symbol
	 *
	 * @return string Total with currency symbol
	 */
	public function calculateTotal()
	{
		$this->_total = 0;	
		$groupedItems = array_count_values($this->_items);

		foreach ($groupedItems as $productCode => $qty){
			if (is_array($this->_pricingSlab[$productCode])){
			    /* calculate price based on price slabs */
				$this->calculatePriceWithSlabs($productCode, $qty);
			} else {
				return "Product Code doesn't esists, please scan correctly.";
			}
		}
		$grandtotal = $this->totalWithCurrencySymbol($this->_total);
		return $grandtotal;
	}

	/**
	 * Return the name of the currently set context.
	 *
	 * @return string Name of the current context
	 */
	private function calculatePriceWithSlabs($productCode, $qty)
	{
		if (
			array_key_exists('volume_price', $this->_pricingSlab[$productCode]) && 
			$qty >= $this->_pricingSlab[$productCode]['volume_price']['qty']
		){
		    /* Get times of price slab can be applied */
			$slabQty = intdiv($qty, $this->_pricingSlab[$productCode]['volume_price']['qty']);
			$this->_total += $slabQty * $this->_pricingSlab[$productCode]['volume_price']['price'];
			
			/* Get remaining qty where pricing slab not apply */
			$remainingQty = $qty - ($slabQty * $this->_pricingSlab[$productCode]['volume_price']['qty']);
			$this->_total += $this->_pricingSlab[$productCode]['unit_price'] * $remainingQty;
		} else {
			$this->_total += $this->_pricingSlab[$productCode]['unit_price'] * $qty;
		}
	}
}